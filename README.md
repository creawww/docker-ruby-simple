# DOCKER RUBY FOR RUBY ON RAILS

Un entorno docker para trabajar con Ruby y Ruby On Rails, con DB sqlLite

contenedor preparado para trabajar con Ruby:2.5.1

Si necesitar otra version simplemente cambia la primera linea del dockerfile

FROM ruby:2.5.1

## construir la imagen si no existe

La primera vez que clonamos el repo necesitamos construir la imagen con nombre propio.

Si construimos la imagen con nombre se quedara en nuestro sistema y podremos usarla para todos los proyectos que la necesitemos

    docker build . -t myruby

## arrancar el contenedor

    docker run -it --rm --name=myapp -p 3000:3000 -v $PWD:/home/myuser/app -v $PWD/_bundle:/usr/local/bundle myruby /bin/bash

Para no tener que escribir un comando tan largo he creado un **runDocker.sh**

    ./runDocker.sh

# crear proyecto en Ruby On rails


entrar al contenedor ./runDocker.sh

Instalar rails

    gem install rails

instalar rails

    rails new . -T

    -T para que no instale test defaul minitest

Add to Gemfile rspec-rails and capybara (test group)

editar el /Gemfile y añadir:

    group :test do
      gem 'rspec-rails'
      gem 'capybara'
    end

actualizar las gemas

    bundle install

instalar la gema de rspec

    rails generate rspec:install

add config to /.rspec

    --require spec_helper
    --format documentation
    --order random
    --color

Ejecutar las migraciones

    rails db:migrate


arrancamos servidor

    rails s

la aplicacion corre en http://localhost:3000

## GENERADORES DE Rails

    rails g scaffold Provider active:boolean name:string nif_cif:string address:string cp:string city:string region:string day:string note:text

    rails generate model Provider active:boolean name:string nif_cif:string address:string cp:string city:string region:string day:string note:text

    rails g controller Providers index

    rails g migration add_provider_id_to_contacts provider:references

Ejecutar migraciones

    rails db:migrate
